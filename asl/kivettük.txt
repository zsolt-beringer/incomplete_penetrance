196		191	al-sayed			Arabic(Saudi)	c.1060C>T		p.Gln354*	c.1060C>T		p.Gln354*	ND	neonatalonsetat3d		mildMR,attentiondeficit,linguisticdelay,hepatomegaly	0
158		156	r�gal2008			unknown	c.617G>T		p.Arg236Gln	c.617G>T		Gly206Val	ND	lateonsetat2m		developmentaldelay,livertransplantationatageof2y	0
228		223	tanaka2002			Japanese	c.1395G>C		p.*465Tyrext*50	c.1395G>C		p.*465Tyrext*50	"<0.006unit/gHb(RBC)
0.003unit/mgProtein(livercells)"	lateonsetat4m			0
111		111	casero2008			unknown	c.532G>A		p.Val178Met	c.839delG		p.Gly280Glufs*4	ND	lateonsetat6m		seizures,psychomotorregression	1
224		219	dursun2008			unknown	c.1334G>C		p.Arg445Pro	c.1334G>C		p.Arg445Pro	ND	lateonsetat2.5y		diedatageof5.5yonchronicpancreatitis	0
190		185	engel2012			unknown	c.943A>G		p.Lys315Glu	c.943A>G		p.Lys315Glu	ND	NBS		severeMR,nohyperammonemia	0
229		11	haberle2004			Druze	c.346C>T		Q116X	c.346C>T		Q116X					0
53		53	h�berle/koch2004			Arabic	c.346C>T		p.Gln116*	c.346C>T		p.Gln116*	ND	neonatalonset		neonatallydeceased	0
175		173	h�berle/koch2004			Arabic(Kuwait)	c.721G>A		p.Glu241Lys	c.721G>A		p.Glu241Lys	ND	neonatalonset		neonatallydeceased	0
