
genes <- c("galt", "mut", "nphs2", "pkhd1", "cftr", "ctns", "nphs1", "asl", "pah", "gaa", "atp7b", "slc26a4", "dhcr7", "idua", "capn3", "pmm2", "galns")

AAto1 <- function(x) {
  if (x == "Gly") return('G')
  if (x == "Ala") return('A')
  if (x == "Leu") return('L')
  if (x == "Met") return('M')
  if (x == "Phe") return('F')
  if (x == "Trp") return('W')
  if (x == "Lys") return('K')
  if (x == "Gln") return('Q')
  if (x == "Glu") return('E')
  if (x == "Ser") return('S')
  if (x == "Pro") return('P')
  if (x == "Val") return('V')
  if (x == "Ile") return('I')
  if (x == "Cys") return('C')
  if (x == "Tyr") return('Y')
  if (x == "His") return('H')
  if (x == "Arg") return('R')
  if (x == "Asn") return('N')
  if (x == "Asp") return('D')
  if (x == "Thr") return('T')
  stop(paste("Bad three-letter AA name", x, "."))
}

AAto3 <- function(x) {
  if (x == 'G') return("Gly")
  if (x == 'A') return("Ala")
  if (x == 'L') return("Leu")
  if (x == 'M') return("Met")
  if (x == 'F') return("Phe")
  if (x == 'W') return("Trp")
  if (x == 'K') return("Lys")
  if (x == 'Q') return("Gln")
  if (x == 'E') return("Glu")
  if (x == 'S') return("Ser")
  if (x == 'P') return("Pro")
  if (x == 'V') return("Val")
  if (x == 'I') return("Ile")
  if (x == 'C') return("Cys")
  if (x == 'Y') return("Tyr")
  if (x == 'H') return("His")
  if (x == 'R') return("Arg")
  if (x == 'N') return("Asn")
  if (x == 'D') return("Asp")
  if (x == 'T') return("Thr")
  stop(paste("Bad one-letter AA name", x, "."))
}

# decides if a character is numeric
is.numeric.char <- function(x) { x %in% 0:9 }

# this function performs a HWE test
# 
# input: a table with patient counts and names are like A/A, A/B,
#        B/B, alleles separated by "/" (obviously, "/" should not
#        appear in allele names)
hwe.test <- function(obs, toprint=T) {
    if (is.na(obs[1]))
        return(list(p.value=NA, text=NA, table=NA))
    
    levs <- unique(unlist(strsplit(names(obs), "/")))
    # if something is non-..., make this the second level
    if (length(levs) == 2 && substr(levs[1], 1, 3) == "non" && substr(levs[2], 1, 3) != "non") {
        tmp <- levs[1]
        levs[1] <- levs[2]
        levs[2] <- tmp
    }

    # number of patients
    N <- sum(obs)
    # observed allele frequencies
    obs.allele <- sapply(levs,
                         function(lev) {
                             n <- 0
                             for (x in strsplit(names(obs), "/")) {
                                 if (x[1] == lev) n <- n + obs[paste(x[1], x[2], sep="/")]
                                 if (x[2] == lev) n <- n + obs[paste(x[1], x[2], sep="/")]
                             }
                             return(n)
                         })
    names(obs.allele) <- levs
    # observed allele relative frequencies
    obs.allele.rel <- obs.allele/sum(obs.allele)
    # expected genotype frequencies
    exp <- rep(NA, length(levs)*(length(levs)+1)/2)
    k <- 1
    for (i in 1:length(levs))
        for (j in i:length(levs)) {
            if (i == j) exp[k] <- N * (obs.allele.rel[i])^2
                if (i != j) exp[k] <- N * (2 * obs.allele.rel[i] * obs.allele.rel[j])
            names(exp)[k] <- paste(levs[i], levs[j], sep="/")
            k <- k + 1
        }
    # calculate p value
    chisq <- sum((obs-exp)^2/exp)
    p.value <- 1-pchisq(chisq, 1)
    # print the results
    if (toprint) {
        cat("Table of genotype counts:\n")
        cat("-----------------------------------------------------------------------------\n")
        print(rbind(obs,exp))
        if (length(obs) == 3) {
            cat(paste("Expected number of ", names(obs)[1], " patients is ", obs[2]^2/(4*obs[3]), ".\n", sep=""))
        }
        cat("\nTable of genotype frequencies:\n")
        cat("-----------------------------------------------------------------------------\n")
    }
    t <- rbind(obs,exp)/N
    if (toprint) {
        print(t)
        cat("-----------------------------------------------------------------------------\n")
        cat(paste("chisq=", chisq, "\n", sep=""))
        cat(paste("p.value=", p.value, "\n", sep=""))
    }
    # showing the direction of the change
    if (all(!is.na(t))) {
        if (t[1,1] >  t[2,1]) arr <- "^"
        if (t[1,1] <= t[2,1]) arr <- "v"
    } else arr <- "?"

    return(list(p.value=p.value, text=paste(dimnames(t)[[2]][1], arr, sep=" "), table=paste(capture.output(print(rbind(obs,exp))), collapse="\n")))
}

# a special hwe test for different/same categories. the arguments are
# two list, one for one allele and its category. NAs are not tolerated
hwe.test.special <- function(cat1, cat2, toprint=T) {
    if (is.na(cat1[1]))
        return(list(p.value=NA, text=NA, table=NA))
    
    cat1 <- unlist(cat1)
    cat2 <- unlist(cat2)

    # levels of categories
    if (length(c(cat1, cat2)) > 0) {
        levs <- sort(unique(c(cat1, cat2)))
    } else {
        levs <- c()
    }
    # if something is non-..., make this the second level
    if (length(levs) == 2 && substr(levs[1], 1, 3) == "non" && substr(levs[2], 1, 3) != "non") {
        tmp <- levs[1]
        levs[1] <- levs[2]
        levs[2] <- tmp
    }
    obs1 <- length(which(cat1 == cat2))
    obs2 <- length(which(cat1 != cat2))
    allele.count <- 2*length(cat1)
    allele.probs <- as.numeric(sapply(levs, function(lev) { length(which(cat1 == lev)) + length(which(cat2 == lev)) })) / allele.count
    exp1 <- sum(allele.probs^2)*allele.count/2
    exp2 <- allele.count/2 - exp1
    obs <- c(obs1, obs2)
    exp <- c(exp1, exp2)
    names(obs) <- c("both muts in same domain", "muts in different domain")
    names(exp) <- c("both muts in same domain", "muts in different domain")
    # calculate p value
    chisq <- sum((obs-exp)^2/exp)
    p.value <- 1-pchisq(chisq, 1)
    # print the results
    if (toprint) {
        cat("Table of genotype counts:\n")
        cat("-----------------------------------------------------------------------------\n")
        print(rbind(obs,exp))
        cat("Table of genotype frequencies:\n")
        cat("-----------------------------------------------------------------------------\n")
    }
    t <- rbind(obs,exp)/allele.count*2
    if (toprint) {
        print(t)
        cat("-----------------------------------------------------------------------------\n")
        cat(paste("chisq=", chisq, "\n", sep=""))
        cat(paste("p.value=", p.value, "\n", sep=""))
    }
    if (!is.na(p.value)) {
        # showing the direction of the change
        if (t[1,1] >  t[2,1]) arr <- "^"
        if (t[1,1] <= t[2,1]) arr <- "v"
        text <- paste(dimnames(t)[[2]][1], arr, sep=" ")
    } else {
        text <- ""
    }
    return(list(p.value=p.value, text=text, table=paste(capture.output(print(rbind(obs,exp))), collapse="\n")))
}

# HWE data
hwe.info <- function(obs, special=F, p.limit) {
    if (is.na(obs[1])) return("NA")
    if (special) {
        if (length(obs$cat1) == 0) return("NA")
        x.hwe <- hwe.test.special(obs$cat1, obs$cat2, toprint=F)
    } else {
        x.hwe <- hwe.test(obs, toprint=F)
    }
    if (is.na(x.hwe$p.value)) return ("NA")
    if (x.hwe$p.value < p.limit) {
        return(paste(x.hwe$text, ", p.value=", format(x.hwe$p.value, digits=3), sep=""))
    } else {
        return(paste("p.value=", format(x.hwe$p.value, digits=3), sep=""))
    }
}

if (Sys.getenv("R_ZIPCMD") == "")
    Sys.setenv(R_ZIPCMD="zip")
library(openxlsx)
    
write.pats <- function(data, filename, sheet.name, add) {
    if (is.null(data)) return()
        
    if (!add)
      unlink(filename)
  
    if (!file.exists(filename)) {
      wb <- createWorkbook(creator="interaction.R")
    } else {
      wb <- loadWorkbook(filename)
    }
    addWorksheet(wb, sheet.name)

    writeData(wb, sheet.name, data)

    grayStyle <- createStyle(fgFill = "gray")
    yellowStyle <- createStyle(fgFill = "yellow")

    addStyle(wb, sheet.name, style=grayStyle, rows=1:dim(data)[1], cols=which(names(data)=="nucls1"))
    addStyle(wb, sheet.name, style=grayStyle, rows=1:dim(data)[1], cols=which(names(data)=="nucls2"))
    addStyle(wb, sheet.name, style=yellowStyle, rows=1:dim(data)[1], cols=which(names(data)=="nucl1"))
    addStyle(wb, sheet.name, style=yellowStyle, rows=1:dim(data)[1], cols=which(names(data)=="nucl2"))
    
    freezePane(wb, sheet.name, firstActiveRow=2, firstActiveCol=3)
    
    saveWorkbook(wb, filename, overwrite=T)
}

# write xls
write.muts <- function(data, filename, sheet.name, add, p.limit.ratio, p.limit.other) {
    if (is.null(data)) return()
        
    if (!add)
      unlink(filename)

    p.value.cols=c()
    i <- 1
    for (nam in names(data)) {
      if (length(grep("p-value", nam, fixed=T)) > 0 && nam != "ratio test p-value")
          p.value.cols <- c(p.value.cols, i)
      i <- i + 1
    }

    if (!file.exists(filename)) {
      wb <- createWorkbook(creator="interaction.R")
    } else {
      wb <- loadWorkbook(filename)
    }
    addWorksheet(wb, sheet.name)

    writeData(wb, sheet.name, data)

    redStyle <- createStyle(fgFill = "red")

    for (col in p.value.cols) {
        for (row in which(as.numeric(data[, col]) < p.limit.other))
            addStyle(wb, sheet.name, style=redStyle, rows=row+1, cols=col)
    }

    # ratio test p-values
    for (col in which(names(data) == "penetrance test p-value")) {
        for (row in which(as.numeric(data[, col]) < p.limit.ratio))
            addStyle(wb, sheet.name, style=redStyle, rows=row+1, cols=col)
    }
    
    # setting column widths
    if (dim(data)[2] >= 1) setColWidths(wb, sheet.name, cols=1, widths=5)
    if (dim(data)[2] >= 2) setColWidths(wb, sheet.name, cols=2, widths=10)
    if (dim(data)[2] >= 3) setColWidths(wb, sheet.name, cols=3:(dim(data)[2]), widths=rep(6, dim(data)[2]-2))
    if (dim(data)[2] >= 4) setColWidths(wb, sheet.name, cols=4, widths=7)
    if (length(which(names(data) == "assoc muts")) > 0) setColWidths(wb, sheet.name, cols=which(names(data) == "assoc muts"), widths=2)
    for (nam in names(data)) {
        if (substr(nam, nchar(nam)-5, nchar(nam)) == ".table" || substr(nam, nchar(nam)-5, nchar(nam)) == " table")
            setColWidths(wb, sheet.name, cols=which(names(data) == nam), widths=2)
    }

    setRowHeights(wb, sheet.name, rows=1, heights=80)
    wrappedStyle <- createStyle(wrapText=T)
    addStyle(wb, sheet.name, style=wrappedStyle, rows=1, cols=1:dim(data)[2])
    
    # freeze panes
    freezePane(wb, sheet.name, firstActiveRow=2, firstActiveCol=9)
    
    saveWorkbook(wb, filename, overwrite=T)
}

write.summary <- function(data, filename) {
    if (is.null(data)) return()

    unlink(filename)
        
    wb <- createWorkbook(creator="interaction.R")
    sheet.name <- "a"
    addWorksheet(wb, sheet.name)
    
    writeData(wb, sheet.name, data)

    for (nam in names(data)) {
        if (substr(nam, nchar(nam)-5, nchar(nam)) == ".table" || substr(nam, nchar(nam)-5, nchar(nam)) == " table")
            setColWidths(wb, sheet.name, cols=which(names(data) == nam), widths=2)
    }

    setRowHeights(wb, sheet.name, rows=1, heights=80)
    wrappedStyle <- createStyle(wrapText=T)
    addStyle(wb, sheet.name, style=wrappedStyle, rows=1, cols=1:dim(data)[2])
    
    freezePane(wb, sheet.name, firstActiveRow=2, firstActiveCol=2)
    
    saveWorkbook(wb, filename, overwrite=T)
}

# add the lowest number i that makes the following test
# significant (and also penetrance <= max.penetrance)
#          pats           ExAc
#   M      het.ac.limit   i
#   LoF    m              n
#
min.ac.for.sign.penetrance.test <- function(ac.lof.exac, ac.lof.pat, max.penetrance, p.limit.ratio, het.ac.limit) {
    if (ac.lof.pat == 0) return(NA)

    i <- ceiling(1/max.penetrance*ac.lof.exac/ac.lof.pat)
    m <- matrix(c(het.ac.limit, ac.lof.pat, i, ac.lof.exac),ncol=2)
    while (fisher.test(m)$p.value > p.limit.ratio && i < 1000000) {
        i <- i + 1
        m[1,2] <- i
    }
    if (i == 1000000) stop("Problem, ask Ambrus!")
    return(i)
}

summary.table.line <- function(x, gene, field.ethnicity.exac, het.ac.limit, p.limit.ratio, p.limit.other, max.penetrance) {
    q <- sum(sapply(x$exac.pathogenic.nucls,
                    function(nucl) {
                        x$get.count.exac.ext(nucl)/x$get.num.exac(field.ethnicity.exac, nucl)
                    }))
    prevalence.of.illness.according.to.exac <- q^2
    
    num.of.collected.pats <- dim(x$patients)[1]
    num.of.european.pats <- length(which(x$patients$ethnicity.exac == "European (Non-Finnish)"))
    AC.het.eu <- sum(x$muts$"AC in ethn het pts with 2 path muts")
    AC.hom.eu <- sum(x$muts$"AC in ethn pts hom for a path mut")
    AC.hom.theo <- 2*sum(x$muts$"num of added theoretical hom pts")

    m <- matrix(c(x$ac.missense.pat, x$ac.trunc.pat, x$ac.missense.exac, x$ac.trunc.exac),ncol=2)
    penetrance.test.p.value <- fisher.test(m)$p.value

    min.exac.AC.for.sign.penetrance.test <-
        min.ac.for.sign.penetrance.test(x$ac.trunc.exac, x$ac.trunc.pat, max.penetrance, p.limit.ratio, het.ac.limit)

#    # proportion of truncating muts in the ethn pat pop and ethn ExAc
#    # in ExAc, we only use those muts which appear in the ethn pat pop
#    # alleles in the ethn pat pop:
#    nucls <- c(x$patients$nucl1[which(!x$patients$excluded)], x$patients$nucl2[which(!x$patients$excluded)])
#    # alleles in theoretical hom pats:
#    for (i in 1:dim(x$muts)[1]) {
#        m <- x$muts$"num of added theoretical hom pts"[i]
#        if (m > 0) {
#            nucls <- c(nucls, rep(x$muts$nucl[i], 2*m))
#        }
#    }
#    # exclude those muts which we don't have truncating/missense data
#
#    if (length(nucls) > 0) {
#        nucls <- nucls[which(sapply(nucls, function(nucl) { x$muts$HWE.truncating.vs.missense[which(x$muts$nucl == nucl)] != "" }))]
#        nums <- sapply(nucls, function(nucl) { if (x$muts$HWE.truncating.vs.missense[which(x$muts$nucl == nucl)] == "truncating") { return(1) } else { return(0) } })
#    } else {
#        nums <- c()
#    }
#    truncating.pat <- sum(nums)
#    missense.pat <- length(nums)-sum(nums)
#    # find in ExAc: unique(nucls)
#    truncating.exac <- 0
#    missense.exac <- 0
#    for (nucl in unique(nucls)) {
#        if (x$muts$HWE.truncating.vs.missense[which(x$muts$nucl == nucl)] == "truncating") {
#            truncating.exac <- truncating.exac + x$get.count.exac.ext(nucl)
#        } else {
#            missense.exac <- missense.exac + x$get.count.exac.ext(nucl)
#        }
#    }
#    truncating.pat.exac.table <-
#        matrix(c(truncating.pat, missense.pat, truncating.exac, missense.exac),  
#               ncol=2,
#               dimnames=list(truncating.vs.missense=c("truncating", "missense"),
#                             group=c("patients", "ExAc")))
#
#    nontranslatable.table <-
#        matrix(c(x$ac.nmd.pat, x$ac.non.nmd.pat, x$ac.nmd.exac, x$ac.non.nmd.exac),  
#               ncol=2,
#               dimnames=list(nontranslatable=c("nontranslatable", "translatable"),
#                             group=c("patients", "ExAc")))

    # create a line for this table
    line <- data.frame(gene=gene,
                       prevalence.of.illness.according.to.exac=prevalence.of.illness.according.to.exac,
                       num.of.collected.pats=num.of.collected.pats,
                       num.of.european.pats=num.of.european.pats,
                       num.of.muts=dim(x$muts)[1],
                       
                       num.of.non.excluded.muts=length(which(!x$muts$excluded)),
                       num.of.muts.above.limit=NA,
                       AC.het.eu=AC.het.eu,
                       AC.hom.eu=AC.hom.eu,
                       AC.hom.theo=AC.hom.theo,
                       AC.trunc.exac=x$ac.trunc.exac,
                       AC.missense.exac=x$ac.missense.exac,
                       AC.trunc.pat=x$ac.trunc.pat,
                       AC.missense.pat=x$ac.missense.pat,
                       penetrance=x$penetrance,
                       penetrance.test.p.value=penetrance.test.p.value,
                       min.exac.AC.for.sign.penetrance.test=min.exac.AC.for.sign.penetrance.test,
                       
                       # nontranslatable.table=paste(capture.output(print(nontranslatable.table)), collapse="\n"),
                       # nontranslatable.p.value=fisher.test(nontranslatable.table)$p.value,
                       # nontranslatable.representation=x$nontranslatable.representation,
                       # truncating.pat=truncating.pat,
                       # missense.pat=missense.pat,
                       # truncating.exac=truncating.exac,
                       # missense.exac=missense.exac,
                       # prop.truncating.pat=truncating.pat/(truncating.pat+missense.pat),
                       # prop.truncating.exac=truncating.exac/(truncating.exac+missense.exac),
                       # truncating.pat.exac.table=paste(capture.output(print(truncating.pat.exac.table)), collapse="\n"),
                       # truncating.pat.exac.p.value=fisher.test(matrix(c(truncating.pat, missense.pat, truncating.exac, missense.exac), ncol=2))$p.value,

                       hwe.null.vs.hypomorphic.table=hwe.test(x$hwe2$HWE.null.vs.hypomorphic, toprint=F)$table,
                       hwe.null.vs.hypomorphic=hwe.info(x$hwe2$HWE.null.vs.hypomorphic, p.limit=p.limit.other),
                       hwe.truncating.vs.missense.table=hwe.test(x$hwe2$HWE.truncating.vs.missense, toprint=F)$table,
                       hwe.truncating.vs.missense=hwe.info(x$hwe2$HWE.truncating.vs.missense, p.limit=p.limit.other),
                       hwe.translatable.table=hwe.test(x$hwe2$HWE.translatable, toprint=F)$table,
                       hwe.translatable=hwe.info(x$hwe2$HWE.translatable, p.limit=p.limit.other),
                       hwe.domain.table=hwe.test(x$hwe2$HWE.domain, toprint=F)$table,
                       hwe.domain=hwe.info(x$hwe2$HWE.domain, p.limit=p.limit.other),
                       hwe.same.different.domain.table=hwe.test.special(x$hwe2$HWE.same.different.domain$cat1, x$hwe2$HWE.same.different.domain$cat2, toprint=F)$table,
                       hwe.same.different.domain=hwe.info(x$hwe2$HWE.same.different.domain, special=T, p.limit=p.limit.other)
                       )
    return(line)
}

summary.table.last.line <- function(lines, hwe2, het.ac.limit, p.limit.ratio, p.limit.other) {
    line <- data.frame(gene="all",
                   prevalence.of.illness.according.to.exac=sum(lines$prevalence.of.illness.according.to.exac),
                   num.of.collected.pats=sum(lines$num.of.collected.pats),
                   num.of.european.pats=sum(lines$num.of.european.pats),
                   num.of.muts=sum(lines$num.of.muts),
                   num.of.non.excluded.muts=sum(lines$num.of.non.excluded.muts),
                   num.of.muts.above.limit=sum(lines$num.of.muts.above.limit),
                   AC.het.eu=sum(lines$AC.het.eu),
                   AC.hom.eu=sum(lines$AC.hom.eu),
                   AC.hom.theo=sum(lines$AC.hom.theo),
                   AC.trunc.exac=sum(lines$AC.trunc.exac),
                   AC.missense.exac=sum(lines$AC.missense.exac),
                   AC.trunc.pat=sum(lines$AC.trunc.pat),
                   AC.missense.pat=sum(lines$AC.missense.pat),
                   penetrance=NA,
                   penetrance.test.p.value=NA,
                   min.exac.AC.for.sign.penetrance.test=NA,

                   # nontranslatable.table=NA,
                   # nontranslatable.p.value=NA,
                   # nontranslatable.representation=NA,
                   # prevalence.of.illness.according.to.exac.wo.nmds=sum(lines$prevalence.of.illness.according.to.exac.wo.nmds),
                   # prevalence.of.illness.according.to.exac.wo.consang.comp=sum(lines$prevalence.of.illness.according.to.exac.wo.consang.comp),
                   # truncating.pat=sum(lines$truncating.pat),
                   # missense.pat=sum(lines$missense.pat),
                   # truncating.exac=sum(lines$truncating.exac),
                   # missense.exac=sum(lines$missense.exac),
                   # prop.truncating.pat=sum(lines$truncating.pat)/sum(lines$truncating.pat+lines$missense.pat),
                   # prop.truncating.exac=sum(lines$truncating.exac)/sum(lines$truncating.exac+lines$missense.exac),
                   # truncating.pat.exac.table=NA,
                   # truncating.pat.exac.p.value=fisher.test(matrix(c(sum(lines$truncating.pat), sum(lines$missense.pat), sum(lines$truncating.exac), sum(lines$missense.exac)), ncol=2))$p.value,

                   hwe.null.vs.hypomorphic.table=hwe.test(hwe2[["HWE.null.vs.hypomorphic"]], toprint=F)$table,
                   hwe.null.vs.hypomorphic=hwe.info(hwe2[["HWE.null.vs.hypomorphic"]], p.limit=p.limit.other),
                   hwe.truncating.vs.missense.table=hwe.test(hwe2[["HWE.truncating.vs.missense"]], toprint=F)$table,
                   hwe.truncating.vs.missense=hwe.info(hwe2[["HWE.truncating.vs.missense"]], p.limit=p.limit.other),
                   hwe.translatable.table=hwe.test(hwe2[["HWE.translatable"]], toprint=F)$table,
                   hwe.translatable=hwe.info(hwe2[["HWE.translatable"]], p.limit=p.limit.other),
                   hwe.domain.table="",
                   hwe.domain="",
                   hwe.same.different.domain.table=hwe.test.special(hwe2[["HWE.same.different.domain"]]$cat1, hwe2[["HWE.same.different.domain"]]$cat2, toprint=F)$table,
                   hwe.same.different.domain=hwe.info(hwe2[["HWE.same.different.domain"]], special=T, p.limit=p.limit.other)
                   )
    return(line)
}

noninf.test <- function(m, p) {
    a <- sum(m[1,])
    b <- sum(m[2,])
    c <- sum(m[,1])

    x1 <- (-(b-c+p*a+p*c)+sqrt((b-c+p*a+p*c)^2+4*(1-p)*p*a*c)) / (2*(1-p))
    x2 <- (-(b-c+p*a+p*c)-sqrt((b-c+p*a+p*c)^2+4*(1-p)*p*a*c)) / (2*(1-p))

    if (x1 > 0 && x1 < a && x1 < c) { x <- x1 } else { x <- x2 }
    if (!(x > 0 && x < a && x < c)) stop("Error 1975, inform Ambrus.")

    m1 <- matrix(c(x,c-x,a-x,a+b-x-(c-x)-(a-x)),ncol=2)

    eq <- function(i, j) {
        return(abs(i - j) < 1e-9)
    }
    
    if (!(eq(sum(m), sum(m1)) &
          eq(sum(m[1,]), sum(m1[1,])) &
          eq(sum(m[2,]), sum(m1[2,])) &
          eq(sum(m[,1]), sum(m1[,1])) &
          eq(sum(m[,2]), sum(m1[,2])) &
          eq(p, (m1[1,1]/m1[2,1])/(m1[1,2]/m1[2,2])))) {
        print(m)
        print(m1)
        stop("Error 1976, inform Ambrus.")
    }

    obs <- m
    exp <- m1
    
    chisq <- sum((obs-exp)^2/exp)
    p.value <- 1-pchisq(chisq, 1)

    return(p.value)
}
