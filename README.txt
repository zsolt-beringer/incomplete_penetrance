Installation in Windows
----------------------------------------------------------------------

In Control Panel, Regional settings, set decimal symbol to "." and
field separator to ",".

To install git, follow this link:

  https://git-scm.com/download/win

Set up ssh keys:

  https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html

Use right click, then "Git Bash here" to start the terminal.

Setup notepad as the editor for git commits:

  git config core.editor notepad

Important git commands:

  git clone git@bitbucket.org:akaposi/nephrogenetics.git
  git pull
  git add filename
  git commit
  git push
  git rm filename


Before running the program
----------------------------------------------------------------------

  install R and Rtools

  you can set the default working directory of R by adding the
  following line to c:\program files\R\R-version\etc\Rprofile.site

    setwd("c:\\users\\user\\documents\\nephrogenetics")

  install.packages("openxlsx")


Running the program
----------------------------------------------------------------------

  source("interaction.R")
  main("nphs2")

The program does not tell you if you need to regenerate
mutationtaster.tsv or vep.txt because of change of input. If you
changed anything in patients.csv (eg. added a new mutation) you will
need to re-run these tools by hand.

You can run the program on several genes at once:

  main(c("nphs1", "nphs2")))

Other ways to run the program:

  main(genes)

  main("nphs2", omit=c("c.686G>A", "c.100C>A"))

Default settings:

  main(genes,
       omit="c.1_852del",
       ethnicity.exac="European (Non-Finnish)",
       het.ac.limit=1,
       p.limit.enrichment=0.05,
       p.limit.ratio=1e-04,
       p.limit.other=0.05,
       p.limit.pair=0.01,
       max.penetrance=1,
       rounds=FALSE,
       pair.test=TRUE,
       exac="gnomad",
       finnish="none",
       enrichment.all.subpop=FALSE,
       max.pen.for.low.pen=0.3,
       min.pen.for.completeness=0.7)

max.penetrance and het.ac.limit is only used for determining the type
of variant with incomplete penetrance

p.limit.* are only used for coloring in the output tables and
  .enrichment is used for determining pathogenicity
  .pair is used for outputting pair tests
  .ratio is used for ratio and penetrance tests

p.limit.ratio should be set to 0.05/(number of enriched variants with
penetrance < max.pen.for.low.pen). this is used to determine the
minimal allele count for a significant penetrance test

p.limit.other is the p-value limit for homozigosity, assoc and HWE
tests

exac="gnomad" or exac="exac"

finnish="none" or finnish="subtract" (subtracts Finnish patients from
ExAc) or finnish=3 (if a mutation has 3x more RAF in the Finnish
population than in ExAc then we exclude it)

If enrichment.all.subpop=TRUE, then the enrichment is defined as
enriched compared to not only the ethnicity.exac subpopulation but
also to the African, East and South Asian and Latino subpopulations.

max.pen.for.low.pen is the maximal penetrance for a variant to be
called a variant with low penetrance.

min.pen.for.completeness is the minimal penentrace for a variant to be
called a complete variant.

You can set zero or more settings, e.g.

  main(genes, p.limit.ratio=0.001)


Output of the program
----------------------------------------------------------------------

output_incompelete.xlsx      table of variants with incomplete penetrance

output_summary.xlsx          summary table (one line for one gene)

output_tests.txt             result of summated HWE tests (all the 
                             genes together) - this will become deprecated

nphs1/output_mutations_round*.xlsx
nphs1/output_patients_round*.xlsx
nphs1/output_incomplete.xlsx


Columns of output_summary.xlsx:

 * gene

 * prevalence.of.illness.according.to.exac

 * num.of.collected.pats = the number of collected patients minus the
   number of pats who carry a mut in omit

 * num.of.muts
 
 * num.of.non.excluded.muts
 
 * num.of.muts.above.limit
 
 * AC.het.eu = AC in het Eu pats w/ two pathogenic muts

 * AC.hom.eu = AC in hom Eu pats w/ two pathogenic muts

 * AC.hom.theo = AC in theoretical hom pats (if hom="2", then
   AC.hom.theo=AC.hom.eu)

 * AC.lof.exac

 * AC.lof.exac.known = AC of those LoF muts in ExAc which appear in
   the patient population (only those which are considered pathogenic
   in the patient popul)

 * AC.lof.pat

 * AC.non.lof.pat

 * penetrance

 * penetrance.test.p.value

 * min.exac.AC.for.sign.penetrance.test

 * HWE...

